require 'byebug'

class Temperature
  attr_accessor :temp_hash

  def initialize(opts = {})
    @temp_hash = opts
  end
  def in_fahrenheit
    if temp_hash.keys.include? :f
      temp_hash[:f]
    elsif temp_hash.keys.include? :c
      temp_hash[:c] * 9 / 5.0 + 32
    end
  end
  def in_celsius
    if temp_hash.keys.include? :c
      temp_hash[:c]
    elsif temp_hash.keys.include? :f
      (temp_hash[:f] - 32) * 5 / 9.0
    end
  end

  def self.from_celsius(temp_in_c)
    Temperature.new(c: temp_in_c)
  end
  def self.from_fahrenheit(temp_in_c)
    Temperature.new(f: temp_in_c)
  end
end

class Celsius < Temperature

  def initialize(temp_in_c)
    @temp_hash = { c: temp_in_c }
  end
end

class Fahrenheit < Temperature

  def initialize(temp_in_f)
    @temp_hash = { f: temp_in_f }
  end
end
