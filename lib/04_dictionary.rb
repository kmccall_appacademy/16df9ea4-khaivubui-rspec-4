require 'byebug'

class Dictionary
  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(addition)
    @entries.merge!(addition) if addition.class == Hash
    @entries[addition] = nil if addition.class == String
  end

  def keywords
    @entries.keys.sort
  end

  def include?(item)
    return true if @entries.keys.include? item
    return false
  end

  def find(key)
    @entries.select { |keys, _| keys.include? key}
  end

  def printable
    string_to_print = ''
    keywords.each do |keyword|
      string_to_print += "[#{keyword}] \"#{@entries[keyword]}\"\n"
    end
    string_to_print[0..-2]
  end
end
