require 'byebug'

class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    seconds = (@seconds % 60).to_s
    seconds = '0' + seconds if seconds.length == 1
    minutes = (@seconds / 60 % 60).to_s
    minutes = '0' + minutes if minutes.length == 1
    hours = (@seconds / 3600).to_s
    hours = '0' + hours if hours.length == 1
    "#{hours}:#{minutes}:#{seconds}"
  end
end
