require 'byebug'
class Book
  attr_reader :title

  def title=(t)
    non_cap_words = %w[and or in of a an the]
    @title = t.split.map do |word|
      if non_cap_words.include? word
        word
      else
        word[0].upcase + word[1..-1]
      end
    end.join(' ')
    @title = @title[0].upcase + @title[1..-1]
  end
end
